<?php
//Credentials for the Database being used in this specific login.php function
$servername = "localhost";
$username = "root";
$password = "tdteam2018";
$dbname = "TDVow";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


//SQL call to the database to match usernamd and password exactly
//$sql = "SELECT buckets.name as name, buckets.description as description, buckets.image as image, buckets.current_price as curr, buckets.total_price as total FROM buckets AS INNER JOIN categories ON categories.id=buckets.category";
$sql = "SELECT buckets.id, buckets.name, buckets.category, buckets.current_price, buckets.total_price FROM buckets JOIN categories ON buckets.category=categories.id";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $curr =  $row["current_price"];
        $total = $row["total_price"];
        $percentint = intval(($curr/$total)*100);
        $percentstr = strval($percentint)+ "%";
        
        echo '<div class="bucketItem">id: ' . $row["id"]. ' - Name: ' . $row["name"]. " " . $row["category"]. '<div><br>';
        echo '<div class="progress">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90"
        aria-valuemin="0" aria-valuemax="100" style="width:'.$percentint. '%">
          40% Complete (success)
        </div>
      </div>';
    }
} else {
    echo "0 results";
}
$conn->close();


?>